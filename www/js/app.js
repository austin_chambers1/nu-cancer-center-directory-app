/*
 * start within Platform ready
 */
$ionicPlatform.ready(function() {
    // register push notification and get local push token
    localStorage.myPush = ''; // I use a localStorage variable to persist the token
    $cordovaPushV5.initialize(  // important to initialize with the multidevice structure !!
        {
            android: {
                senderID: "1111111111"
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false',
                clearBadge: true
            },
            windows: {}
        }
    ).then(function (result) {
        $cordovaPushV5.onNotification();
        $cordovaPushV5.onError();
        $cordovaPushV5.register().then(function (resultreg) {
            localStorage.myPush = resultreg;
            // SEND THE TOKEN TO THE SERVER, best associated with your device id and user
        }, function (err) {
            // handle error
        });
    });
});

/*
 * Push notification events
 */
$rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {  // use two variables here, event and data !!!
    if (data.additionalData.foreground === false) {
        // do something if the app is in foreground while receiving to push - handle in app push handling
        }
    } else {
       // handle push messages while app is in background or not started
    }
    if (Device.isOniOS()) {
        if (data.additionalData.badge) {
            $cordovaPushV5.setBadgeNumber(NewNumber).then(function (result) {
                // OK
            }, function (err) {
                // handle error
            });
        }
    }

    $cordovaPushV5.finish().then(function (result) {
        // OK finished - works only with the dev-next version of pushV5.js in ngCordova as of February 8, 2016
    }, function (err) {
        // handle error
    });
});

$rootScope.$on('$cordovaPushV5:errorOccurred', function(event, error) {
    // handle error
});



var data = {"DATA": [{"PROFILEIMAGE": "http://image.shutterstock.com/display_pic_with_logo/160669/160669,1298475214,1/stock-photo-profile-of-a-handsome-man-71850487.jpg", "FIRSTNAME": "Todd", "DEGREE": "MD", "TITLE": "Professor", "LASTNAME": "Jones", "MIDDLEINITIAL": "M.", "TELEPHONE": "420.424.4242", "SPECIALTY": "Stock Photos", "DEPARTMENT": "Internet", "EMAIL": "pleasehelpme@u.northwestern.edu", "NETID": "abc123"}, {"PROFILEIMAGE": "http://previews.123rf.com/images/awesomeshotz/awesomeshotz1301/awesomeshotz130100056/17180809-Profile-of-a-Beautiful-African-American-Woman-Stock-Photo.jpg", "FIRSTNAME": "Sarah", "DEGREE": "MD", "TITLE": "Scientist", "LASTNAME": "Smith", "MIDDLEINITIAL": "null", "TELEPHONE": "470.696.4242", "SPECIALTY": "science", "DEPARTMENT": "science", "EMAIL": "ILikeCartoons23@gmail.com", "NETID": "Lol990"}]};


var name_array = [];
var name_dict = {};

data.DATA.forEach(function(entry){
  if(!(entry['NETID'] in name_dict)){
    name_dict = [];
    name_dict['title'] = entry["FIRSTNAME"];
    name_dict['image'] = entry["PROFILEIMAGE"];
    name_dict['last_name'] = entry["LASTNAME"];
    if (entry["TELEPHONE"] === 'null'){
      name_dict['phone'] = "n/a";
    }
    else{
      name_dict['phone'] = entry["TELEPHONE"];
    }
    name_array.push(name_dict);
  }
});

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.controller('usersCtrl', function($scope) {
  $scope.tasks = [];
  name_array.forEach(function(entry){
    $scope.tasks.push({'image': entry['image'], 'title': entry['title'], 'last': entry['last_name'], 'phone': entry['phone']});
  });
});